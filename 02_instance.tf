# Creamos la instancia de scaleway

resource "scaleway_instance_server" "pixelfed" {
	image = "b4bdbee1-e1f1-4436-8de4-bdb1b6ba4803"
	name = "pixelfed"
	zone = "fr-par-1"
	type = "ARM64-2GB"
	cloud_init = "${file("userdata.yaml")}"
}
